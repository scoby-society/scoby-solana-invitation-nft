import { Fragment, useRef, useState, useEffect } from 'react';
import { useWallet, useConnection } from "@solana/wallet-adapter-react";
import {
  Connection,
  Keypair,
  PublicKey,
  Transaction,
  TransactionInstruction,
  ConfirmOptions,
  LAMPORTS_PER_SOL,
  SystemProgram,
  clusterApiUrl,
  SYSVAR_RENT_PUBKEY,
  SYSVAR_CLOCK_PUBKEY
} from '@solana/web3.js'
import {AccountLayout,MintLayout,TOKEN_PROGRAM_ID,ASSOCIATED_TOKEN_PROGRAM_ID,Token} from "@solana/spl-token";
import useNotify from './notify'
import * as bs58 from 'bs58'
import * as anchor from "@project-serum/anchor";
import { programs } from '@metaplex/js';
import axios from "axios"
import {WalletConnect, WalletDisconnect} from '../wallet'
import { Container, Snackbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { CircularProgress, Card, CardMedia, Grid, CardContent, Typography, BottomNavigation,
				Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper  } from '@mui/material'
import {createMint,createAssociatedTokenAccountInstruction,sendTransactionWithRetry} from './utility'

let wallet : any
let conn = new Connection(clusterApiUrl('devnet'))
let notify: any

const { metadata: { Metadata } } = programs
const TOKEN_METADATA_PROGRAM_ID = new anchor.web3.PublicKey("metaqbxxUerdq28cj1RbAWkYQm3ybzjb6a8bt518x1s")
const programId = new PublicKey('FyRNNu3ZH1Y6poZL7QYg2Fo1Br79kHQU5HdZe4d6wo7K')
const POOL = new PublicKey('EjyMN4SXWnAYokEF57KQVAo2dtCYUTEbnxd8axeUkNqu')
const SYMBOL = "Invitation"
const idl = require('./invitation.json')

const nfpProgramId = new PublicKey('EJwH4UAsvAbunQtG2srAtULxJh1vGkzVFMeaikBnytiZ')
const nfpPOOL = new PublicKey('A1tkAdTRDTGyzSFYmsmL3jYkj9Py6dJ3xtdCGXX12Foa')
const nfpSYMBOL = "NFP"
const nfpIdl = require('./nfp-nft.json')


const confirmOption : ConfirmOptions = {commitment : 'finalized',preflightCommitment : 'finalized',skipPreflight : false}

interface Schedule{
	time : string;
	amount : string;
}

let defaultSchedule = {
	time : '', amount : ''
}

interface AlertState {
  open: boolean;
  message: string;
  severity: 'success' | 'info' | 'warning' | 'error' | undefined;
}

export default function Mint(){
	wallet = useWallet()
	notify = useNotify()

	const [pool, setPool] = useState<PublicKey>(POOL)
	const [alertState, setAlertState] = useState<AlertState>({open: false,message: '',severity: undefined})
    const [isProcessing, setIsProcessing] = useState(false)
    const [holdingNfts, setHoldingNfts] = useState<any[]>([])
	const [poolData, setPoolData] = useState<any>(null)

	useEffect(()=>{
		getPoolData()
	},[pool])

	useEffect(()=>{
		if(poolData != null && wallet.publicKey != null){
			getNfpNftsForOwner(wallet.publicKey, nfpSYMBOL)
		}
	},[wallet.publicKey,poolData])

	const getTokenWallet = async (owner: PublicKey,mint: PublicKey) => {
	  return (
	    await PublicKey.findProgramAddress(
	      [owner.toBuffer(), TOKEN_PROGRAM_ID.toBuffer(), mint.toBuffer()],
	      ASSOCIATED_TOKEN_PROGRAM_ID
	    )
	  )[0];
	}
	const getMetadata = async (mint: PublicKey) => {
	  return (
	    await anchor.web3.PublicKey.findProgramAddress(
	      [
	        Buffer.from("metadata"),
	        TOKEN_METADATA_PROGRAM_ID.toBuffer(),
	        mint.toBuffer(),
	      ],
	      TOKEN_METADATA_PROGRAM_ID
	    )
	  )[0];
	}
	const getEdition = async (mint: PublicKey) => {
	  return (
	    await anchor.web3.PublicKey.findProgramAddress(
	      [
	        Buffer.from("metadata"),
	        TOKEN_METADATA_PROGRAM_ID.toBuffer(),
	        mint.toBuffer(),
	        Buffer.from("edition")
	      ],
	      TOKEN_METADATA_PROGRAM_ID
	    )
	  )[0];
	}

	const getPoolData = async() => {
		try{
			const poolAddress = new PublicKey(pool)
			const randWallet = new anchor.Wallet(Keypair.generate())
			const provider = new anchor.Provider(conn,randWallet,confirmOption)
			const program = new anchor.Program(idl,programId,provider)
			const pD = await program.account.pool.fetch(poolAddress)
    		setPoolData(pD)
		} catch(err){
			console.log(err)
			setPoolData(null)
		}
	}

	// async function getNftsForOwner(owner : PublicKey,symbol : string) {
	// 	let allTokens: any[] = []
	// 	const tokenAccounts = await conn.getParsedTokenAccountsByOwner(owner, {programId: TOKEN_PROGRAM_ID},"finalized");
	// 	const randWallet = new anchor.Wallet(Keypair.generate())
	// 	const provider = new anchor.Provider(conn,randWallet,confirmOption)
	// 	const program = new anchor.Program(idl,programId,provider)

	// 	for (let index = 0; index < tokenAccounts.value.length; index++) {
	// 		try{
	// 			const tokenAccount = tokenAccounts.value[index];
	// 			const tokenAmount = tokenAccount.account.data.parsed.info.tokenAmount;

	// 			if (tokenAmount.amount == "1" && tokenAmount.decimals == "0") {
	// 				let nftMint = new PublicKey(tokenAccount.account.data.parsed.info.mint)
	// 				let pda = await getMetadata(nftMint)
	// 				const accountInfo: any = await conn.getParsedAccountInfo(pda);
	// 				let metadata : any = new Metadata(owner.toString(), accountInfo.value)
					
	// 				if (metadata.data.data.symbol == symbol) {
	// 					let [metadataExtended, bump] = await PublicKey.findProgramAddress([nftMint.toBuffer(), pool.toBuffer()],programId)

	// 					if((await conn.getAccountInfo(metadataExtended)) == null) continue;
	// 					let extendedData = await program.account.metadataExtended.fetch(metadataExtended)
	// 					let [parentMetadataExtended, bump2] = await PublicKey.findProgramAddress([extendedData.parentInvitation.toBuffer(), pool.toBuffer()],programId)
	// 					let parentExtendedData = await program.account.metadataExtended.fetch(parentMetadataExtended)
						
	// 					const { data }: any = await axios.get(metadata.data.data.uri)
	// 					const entireData = { ...data, id: Number(data.name.replace( /^\D+/g, '').split(' - ')[0])}

	// 					allTokens.push({
	// 						mint : nftMint, metadata : pda, tokenAccount :  tokenAccount.pubkey,
	// 						metadataExtended : metadataExtended, extendedData : extendedData,
	// 						data : metadata.data.data, offChainData : entireData, parentId : parentExtendedData.number})
	// 				}
	// 			}
	// 		} 
	// 		catch(err) {
	// 			continue;
	// 		}
	// 	}
	// 	allTokens.sort(function(a:any, b: any){
	// 		if(a.extendedData.number < b.extendedData.number) {return -1;}
	// 		if(a.extendedData.number > b.extendedData.number) {return 1;}
	// 		return 0;
	// 	})
	// 	setHoldingNfts(allTokens)
	// 	return allTokens
	// }


	async function getNfpNftsForOwner(owner : PublicKey,	symbol : string) {
		let allTokens: any[] = []
		const tokenAccounts = await conn.getParsedTokenAccountsByOwner(owner, {programId: TOKEN_PROGRAM_ID},"finalized");
		console.log(tokenAccounts);
		const randWallet = new anchor.Wallet(Keypair.generate())
		const provider = new anchor.Provider(conn,randWallet,confirmOption)
		const nfpProgram = new anchor.Program(nfpIdl,nfpProgramId,provider)
  
		for (let index = 0; index < tokenAccounts.value.length; index++) {
			try{
				const tokenAccount = tokenAccounts.value[index];
				const tokenAmount = tokenAccount.account.data.parsed.info.tokenAmount;

				if (tokenAmount.amount == "1" && tokenAmount.decimals == "0") {
					let nftMint = new PublicKey(tokenAccount.account.data.parsed.info.mint)
					let pda = await getMetadata(nftMint)
					const accountInfo: any = await conn.getParsedAccountInfo(pda);
					let metadata : any = new Metadata(owner.toString(), accountInfo.value)
					
					if (metadata.data.data.symbol == symbol) {
						let [metadataExtended, bump] = await PublicKey.findProgramAddress([nftMint.toBuffer(), nfpPOOL.toBuffer()],nfpProgramId)

						if((await conn.getAccountInfo(metadataExtended)) == null) continue;

						let extendedData = await nfpProgram.account.metadataExtended.fetch(metadataExtended)
						// const { data }: any = await axios.get(metadata.data.data.uri)
						// const entireData = { ...data, id: Number(data.name.replace( /^\D+/g, '').split(' - ')[0])}

						allTokens.push(
							{
								mint : nftMint,
								metadata : pda, 
								tokenAccount :  tokenAccount.pubkey,
								metadataExtended : metadataExtended, 
								extendedData : extendedData,
								data : metadata.data.data,
								// offChainData : entireData, 
							}
						)
					}
				}
			} 
			catch(err) {
				continue;
			}
		}
		allTokens.sort(function(a:any, b: any){
			if(a.extendedData.number < b.extendedData.number) {return -1;}
			if(a.extendedData.number > b.extendedData.number) {return 1;}
			return 0;
		})
		console.log(allTokens)
		setHoldingNfts(allTokens)
		return allTokens
	}

	const mint = async() =>{
		try{
			const provider = new anchor.Provider(conn, wallet as any, confirmOption)
			const program = new anchor.Program(idl,programId,provider)
			const poolData = await program.account.pool.fetch(pool)
			const configData = await program.account.config.fetch(poolData.config)

			let transaction = new Transaction()
			let instructions : TransactionInstruction[] = []
			let signers : Keypair[] = []
			const mintRent = await conn.getMinimumBalanceForRentExemption(MintLayout.span)
			const mintKey = createMint(instructions, wallet.publicKey,mintRent,0,wallet.publicKey,wallet.publicKey,signers)
			const recipientKey = await getTokenWallet(wallet.publicKey, mintKey)
			createAssociatedTokenAccountInstruction(instructions,recipientKey,wallet.publicKey,wallet.publicKey,mintKey)
			instructions.push(Token.createMintToInstruction(TOKEN_PROGRAM_ID,mintKey,recipientKey,wallet.publicKey,[],1))
			instructions.forEach(item=>transaction.add(item))
			const metadata = await getMetadata(mintKey)
			const masterEdition = await getEdition(mintKey)
			const [metadataExtended, bump] = await PublicKey.findProgramAddress([mintKey.toBuffer(),pool.toBuffer()], programId)
			// let tx = new Transaction()
			
			if(poolData.countMinting == 0){
				transaction.add(program.instruction.mintRoot(new anchor.BN(bump),{
					accounts : {
						owner : wallet.publicKey,
						pool : pool,
						config : poolData.config,
						nftMint : mintKey,
						nftAccount : recipientKey,
						metadata : metadata,
						masterEdition : masterEdition,
						metadataExtended : metadataExtended,
						// relatedNfpMint : relatedNfp,
						// relatedNfpAccount : relatedNfpAccount,
						scobyWallet : poolData.scobyWallet,
						tokenProgram : TOKEN_PROGRAM_ID,
						tokenMetadataProgram : TOKEN_METADATA_PROGRAM_ID,
						systemProgram : SystemProgram.programId,
						rent : SYSVAR_RENT_PUBKEY,
					}
				}))
			}else{
				let nfpNfts = await getNfpNftsForOwner(wallet.publicKey, nfpSYMBOL)
				if(nfpNfts.length==0) throw new Error("You do not have any parent NFP")
				let relatedNfp = nfpNfts[0]
				
				const nfpResp = await conn.getTokenLargestAccounts(relatedNfp.extendedData.mint, 'finalized')
				if(nfpResp==null || nfpResp.value==null || nfpResp.value.length==0) throw new Error("Invalid NFP")
				const relatedNfpAccount = nfpResp.value[0].address

				const parentNfpResp = await conn.getTokenLargestAccounts(relatedNfp.extendedData.parentNfp, 'finalized')
				if(parentNfpResp==null || parentNfpResp.value==null || parentNfpResp.value.length==0) throw new Error("Invalid NFP")
				const parentNfpAccount = parentNfpResp.value[0].address
				const info = await conn.getAccountInfo(parentNfpAccount, 'finalized')
				if(info == null) throw new Error('Parent NFP info failed');
				const accountInfo = AccountLayout.decode(info.data)
				if(Number(accountInfo.amount)==0) throw new Error("Invalid Parent Nfp info")
				const parentNfpOwner = new PublicKey(accountInfo.owner)


				
				// let [relatedInvitationMetadataExtended, invitationBump] = await PublicKey.findProgramAddress([relatedNfp.extendedData.relatedInvitation.toBuffer(), pool.toBuffer()],programId)
				// let relatedInvitationNftextendedData = await program.account.metadataExtended.fetch(relatedInvitationMetadataExtended)

				// const respOldest = await conn.getTokenLargestAccounts(relatedInvitationNftextendedData.mint, 'finalized')
				// if(respOldest==null || respOldest.value==null || respOldest.value.length==0) throw new Error("Invalid parent")
				// const oldestNftAccount = respOldest.value[0].address

				// // const parentNftAccount = await getTokenWallet(wallet.publicKey, oldestNft.extendedData.parent)
				// const resp = await conn.getTokenLargestAccounts(relatedInvitationNftextendedData.parentInvitation, 'finalized')
				// if(resp==null || resp.value==null || resp.value.length==0) throw new Error("Invalid parent")
				// const parentNftAccount = resp.value[0].address
				// const info = await conn.getAccountInfo(parentNftAccount, 'finalized')
				// if(info == null) throw new Error('Parent NFT info failed');
				// const accountInfo = AccountLayout.decode(info.data)
				// if(Number(accountInfo.amount)==0) throw new Error("Invalid Parent info")
				// const parentNftOwner = new PublicKey(accountInfo.owner)


				// let nfts = await getNftsForOwner(wallet.publicKey, SYMBOL)
				// if(nfts.length==0) throw new Error("You do not have any parent NFT")
				// let oldestNft = nfts[0]
				// // const parentNftAccount = await getTokenWallet(wallet.publicKey, oldestNft.extendedData.parent)
				// const resp = await conn.getTokenLargestAccounts(oldestNft.extendedData.parentInvitation, 'finalized')
				// if(resp==null || resp.value==null || resp.value.length==0) throw new Error("Invalid parent")
				// const parentNftAccount = resp.value[0].address
				// const info = await conn.getAccountInfo(parentNftAccount, 'finalized')
				// if(info == null) throw new Error('Parent NFT info failed');
				// const accountInfo = AccountLayout.decode(info.data)
				// if(Number(accountInfo.amount)==0) throw new Error("Invalid Parent info")
				// const parentNftOwner = new PublicKey(accountInfo.owner)

				

				transaction.add(program.instruction.mint(new anchor.BN(bump),{
					accounts : {
						owner : wallet.publicKey,
						pool : pool,
						config : poolData.config,
						nftMint : mintKey,
						nftAccount : recipientKey,
						metadata : metadata,
						masterEdition : masterEdition,
						metadataExtended : metadataExtended,
						// oldestNftMint : relatedInvitationNftextendedData.mint,
						// oldestNftAccount : oldestNftAccount,
						// oldestMetadataExtended : relatedInvitationMetadataExtended,
						relatedNfpMint : relatedNfp.extendedData.mint,
						relatedNfpAccount : relatedNfpAccount,
						parentNfpMint : relatedNfp.extendedData.parentNfp,
						parentNfpAccount : parentNfpAccount,
						parentNfpOwner : parentNfpOwner,
						scobyWallet : poolData.scobyWallet,
						tokenProgram : TOKEN_PROGRAM_ID,
						tokenMetadataProgram : TOKEN_METADATA_PROGRAM_ID,
						systemProgram : SystemProgram.programId,
						rent : SYSVAR_RENT_PUBKEY,					
					}
				}))
			}
			// await sendTransaction(tx,[])
			await sendTransaction(transaction,signers)
			setAlertState({open: true, message:"Congratulations! Succeeded!",severity:'success'})
			await getPoolData()
		}catch(err){
			console.log(err)
			setAlertState({open: true, message:"Failed! Please try again!",severity:'error'})
		}
	}

	async function sendTransaction(transaction : Transaction, signers : Keypair[]) {
		transaction.feePayer = wallet.publicKey
		transaction.recentBlockhash = (await conn.getRecentBlockhash('max')).blockhash;
		await transaction.setSigners(wallet.publicKey,...signers.map(s => s.publicKey));
		if(signers.length != 0) await transaction.partialSign(...signers)
		const signedTransaction = await wallet.signTransaction(transaction);
		let hash = await conn.sendRawTransaction(await signedTransaction.serialize());
		await conn.confirmTransaction(hash);
		return hash
	}

	return <>
		<main className='content'>
			<div className="card">
			{
				poolData != null && 
				<h6 className="card-title">Mint Invitation: {poolData.countMinting+ " NFIs were minted"}</h6>
			}
				<form className="form">
					{
						(wallet && wallet.connected) &&
						<button type="button" disabled={isProcessing==true} className="form-btn" style={{"justifyContent" : "center"}} onClick={async ()=>{
							setIsProcessing(true)
							setAlertState({open: true, message:"Processing transaction",severity: "warning"})
							await mint()
							setIsProcessing(false)
						}}>
							{ isProcessing==true ? "Processing..." :"Mint" }
						</button>
					}
					<WalletConnect/>
				</form>
			</div>
			<Grid container spacing={1}>
			{
				holdingNfts.map((item, idx)=>{
					return <Grid item xs={2}>
						<Card key={idx} sx={{minWidth : 300}}>
							{/* <CardMedia component="img" height="200" image={item.offChainData.image} alt="green iguana"/> */}
							<CardContent>
								<Typography gutterBottom variant="h6" component="div">
								{item.data.name}
								</Typography>
								<Typography variant="body2" color="text.secondary">
								{"mint : " + item.extendedData.mint}
								</Typography>
								<Typography variant="body2" color="text.secondary">
								{"parent : " + item.extendedData.parentNfp}
								</Typography>
								<Typography variant="body2" color="text.secondary">
								{"grandparent : "+ item.extendedData.grandParentNfp}
								</Typography>
								<Typography variant="body2" color="text.secondary">
								{"Followers : " + item.extendedData.childrenCount}
								</Typography>
							</CardContent>
						</Card>
					</Grid>
				})
			}
			</Grid>
			<Snackbar
        open={alertState.open}
        autoHideDuration={alertState.severity != 'warning' ? 6000 : 1000000}
        onClose={() => setAlertState({ ...alertState, open: false })}
      >
        <Alert
        	iconMapping={{warning : <CircularProgress size={24}/>}}
          onClose={() => setAlertState({ ...alertState, open: false })}
          severity={alertState.severity}
        >
          {alertState.message}
        </Alert>
      </Snackbar>
		</main>
	</>
}